# webcam

Originally inspired by ```https://github.com/nswerhun/rice/blob/master/scripts/webcam```, webcam is a tool that makes it easy to manage droidcam commands.

## Installation

Make sure you have:
1. droidcam
2. mpv (optional - for camera playback)

### Install Process

1. Download the repo using ```git clone https://gitlab.com/RealStoneSword/webcam.git``` and CD into the folder.
2. Move ```webcam``` to /usr/bin
3. Done!

## Enviroment Variables

DROIDCAM_PORT - The port you want droidcam to use
